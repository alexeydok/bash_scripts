#!/usr/bin/bash

function hello { 
  echo "Hello ${1}" 
} 

function testing { 
  echo "This is a testing world, $@" 
}

case ${1} in 
	test*) testing ${@:2};; 
	hello*) hello ${2} ;; 
	*) echo "!!!!";;
esac

